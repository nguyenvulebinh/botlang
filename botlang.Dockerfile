FROM ubuntu:bionic

LABEL maintainer="Khiem Doan <doankhiem.crazy@gmail.com>"

ENV DEBIAN_FRONTEND noninteractive
ENV DEBCONF_NONINTERACTIVE_SEEN true
ENV LC_ALL C.UTF-8
ENV LANG C.UTF-8
ENV TZ=Asia/Ho_Chi_Minh

# install web service
COPY requirements.txt /src/requirements.txt
COPY docker/main.py /src/main.py
RUN buildDeps='python3-pip wget gcc python3-dev libssl-dev libdpkg-perl zlib1g-dev' \
    && apt-get update \
    && apt-get install -y --no-install-recommends python3 libssl1.1 \
    && apt-get install -y --no-install-recommends $buildDeps \
    && pip3 install setuptools --no-cache-dir \
    && pip3 install gunicorn gevent --no-cache-dir \
    && pip3 install -r /src/requirements.txt --no-cache-dir \
    && apt-get purge -y --auto-remove $buildDeps \
    && rm -rf /var/lib/apt/lists/* /var/cache/apt/* \
    && ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# use for debug
ENV FLASK_APP /workspace/src/main.py
ENV FLASK_DEBUG 1

VOLUME /workspace
WORKDIR /workspace/src
EXPOSE 8800
ENTRYPOINT gunicorn main:app \
    --bind=0.0.0.0:8800 --workers=1 --worker-class=gevent
