import os
import sys

current_dir = os.path.dirname(__file__)
parent_dir = os.path.dirname(current_dir)
sys.path.insert(0, parent_dir)

from src.core import core_interface

if __name__ == '__main__':
    core_interface.train_botlang()
