import os
import sys

current_dir = os.path.dirname(__file__)
parent_dir = os.path.dirname(current_dir)
sys.path.insert(0, parent_dir)

from src.config import *
from flask import Flask, request
import json
import logging
import traceback
from src.util import utils_server
from src.util import utils_fb_messenger
from src.core import core_interface
import coloredlogs

app = Flask(__name__)


@app.route('/webhook/messenger', methods=['GET'])
def handle_verification():
    """
    verify token cho webhook
    :return: token de verify
    """
    if request.args.get('hub.verify_token', '') == VERIFY_TOKEN:
        print("Verified")
        return request.args.get('hub.challenge', '')
    else:
        print("Wrong token")
        return "Error, wrong validation token"


@app.route('/webhook/messenger', methods=['POST'])
def handle_message():
    """
    Xu ly khi nhan tin nhan tu facebook messenger
    :return: ok
    """
    return "ok", 200


@app.route('/handle-webhook-message', methods=['POST'])
def handle_webhook_message():
    def get_handle_message(data):
        try:
            if data.get("object") and data.get("object") == "page":
                if data.get('entry') and len(data.get('entry')) > 0:
                    entry = data.get('entry')
                    if entry[0].get('messaging'):
                        return handle_message_messenger
                    elif entry[0].get('changes') and len(entry[0].get('changes')) > 0:
                        changes = entry[0].get('changes')
                        if changes[0].get('field') and changes[0].get('field') == 'feed':
                            return handle_message_feed
        except Exception as e:
            logging.warning(e)
            tb = traceback.format_exc()
            logging.error("get_handle_message {}".format(tb))
        return handle_default

    json_data = request.get_json()
    logging.info("handle_webhook_message {}".format(json.dumps(json_data)))

    get_handle_message(json_data.get('data'))(json_data.get('data'))
    return json.dumps({'result': 'success'})


def handle_message_feed(data):
    logging.info("handle_message_feed {}".format(json.dumps(data)))
    try:
        if data["object"] == "page":
            for entry in data["entry"]:
                page_id = entry.get('id')
                changes = entry.get('changes')
                for change_data in changes:
                    if change_data.get('field') == 'feed':
                        value_content = change_data.get('value')
                        if value_content.get('item') == 'comment':
                            user_id = value_content.get('from').get('id')
                            message = value_content.get('message')
                            debug_message(page_id, user_id, message)

    except Exception as e:
        logging.warning(e)
        tb = traceback.format_exc()
        logging.error("handle_message_feed_error {}".format(tb))
    return "ok", 200


def debug_message(page_id, user_id, message):
    if message.startswith("#translate"):
        from googletrans import Translator
        content = message[len('#translate'):]
        message_sent = "{} in {} is {}".format(content, 'Vietnamese', Translator().translate(content, 'vi').text)
        utils_fb_messenger.send_message(user_id, page_id, message_sent)
    pass


def handle_default(data):
    logging.info("handle_default {}".format(json.dumps(data)))
    return "ok", 200


def handle_message_messenger(data):
    """
    Xu ly khi nhan tin nhan tu facebook messenger
    :return: ok
    """
    logging.info("handle_message_messenger {}".format(json.dumps(data)))
    try:
        if data["object"] == "page":
            for entry in data["entry"]:
                timestamp = entry['time']
                for messaging_event in entry["messaging"]:
                    sender_id = messaging_event["sender"]["id"]
                    recipient_id = messaging_event["recipient"]["id"]
                    # utils_fb_messenger.send_typing(sender_id, recipient_id)
                    if messaging_event.get("message"):
                        mid = messaging_event.get("message").get("mid")
                        if messaging_event["message"].get("quick_reply"):
                            # Nguoi dung click vao quick button
                            postback = json.loads(messaging_event["message"]["quick_reply"]["payload"])
                            if postback["type"] == "QUICK_REPLY":
                                # Nguoi dung click nut tra loi nhanh
                                pass
                        elif messaging_event["message"].get("is_echo"):
                            # Su kien xay ra khi co tin nhan phat sinh tu page (admin hoac bot)
                            if messaging_event["message"].get("text"):
                                # Neu event xay ra khi tin nhan text duoc gui
                                message_text = messaging_event["message"]["text"]
                            else:
                                # Neu gui file hoac cac template khac
                                message_text = "attachments or other"
                            if messaging_event["message"].get("metadata") and \
                                messaging_event["message"]["metadata"] == "BOT_MESSENGE":
                                # Tin nhan duoc gui tu page la do BOT
                                pass
                            else:
                                # Tin nhan duoc gui tu page la do doi cskh
                                pass
                        else:
                            # Truong hop ton tai message trong messaging
                            message = messaging_event["message"]
                            if messaging_event["message"].get("text"):
                                # Neu noi dung cua message chi co text
                                message_text = messaging_event["message"]["text"]

                                server_response = utils_server.handle_message(
                                    utils_fb_messenger.get_json_obj_chat_text(sender_id,
                                                                              recipient_id,
                                                                              timestamp,
                                                                              message))
                                fb_access_token = PAGE_ACCESS_TOKEN[recipient_id]
                                logging.info("fb_access_token {}".format(fb_access_token))
                                core_interface.get_agent_input_channel().process_data_messenger(fb_access_token, data)
                                # return handel_server_response(sender_id, recipient_id, server_response)

                            if messaging_event["message"].get("attachments"):
                                type_attachments = message.get("attachments")
                                if type_attachments and type_attachments[0].get("type") == 'file':
                                    # Neu noi dung cua message la file
                                    pass
                                elif type_attachments and type_attachments[0].get("type") == 'image':
                                    # Neu noi dung cua message la image
                                    pass
                    if messaging_event.get("postback"):
                        # Nguoi dung click nut trong menu, START
                        postback = json.loads(messaging_event["postback"]["payload"])
                        if postback["type"] == "GET_STARTED":
                            # Nguoi dung click vao nut start khi bat dau cuoc tro chuyen
                            messaging_start_data = messaging_event.get("postback")
                            if messaging_start_data.get("referral") and messaging_start_data.get("referral").get("ref"):
                                # Nguoi dung click short link có ref
                                ref = json.loads(messaging_start_data.get("referral").get("ref"))
                                pass
                            else:
                                # Link khong co ref ma nguoi dung an GET_START
                                pass
                        elif postback["type"] == "RESTART_BOT":
                            # Nguoi dung click bat dau tro chuyen
                            pass
                    if messaging_event.get("optin"):
                        # Nguoi dung click nut messenger tren website
                        pass

                    if messaging_event.get("referral"):
                        # Nguoi dung click short link có ref
                        if messaging_event.get("referral").get("ref"):
                            ref = json.loads(messaging_event.get("referral").get("ref"))
                            pass
    except Exception as e:
        logging.warning(e)
        tb = traceback.format_exc()
        logging.error("handle_message_error {}".format(tb))
        utils_server.print_log("handle_message_error", tb)
    return "ok", 200


def handle_bot_response(sender_id, recipient_id, response):
    """
    Xu ly response bot tra ve
    :param sender_id: id nguoi gui
    :param recipient_id: id nguoi nhan
    :param response: json object
    :return: None
    """
    utils_server.print_log("handle_bot_response", "start")
    try:
        pass
    except Exception as e:
        logging.warning(e)
        tb = traceback.format_exc()
        utils_server.print_log("handle_bot_response_error", tb)
    utils_server.print_log("handle_bot_response", "end")


# @app.route('/test/<text>', methods=['GET'])
# def test(text):
#     return str(core_interface.get_agent_input_channel().process_message(text))


coloredlogs.install()
core_interface.init_agent()

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8800, debug=True)
