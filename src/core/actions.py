import logging
from rasa_core.actions.forms import FormAction, FreeTextFormField, EntityFormField
from rasa_core.actions import Action
from rasa_core.events import SlotSet
from googletrans import Translator

logger = logging.getLogger(__name__)
import requests

translator = Translator()


class TranslateAPI():
    def search(self, content, target_language):
        logging.info("Translate {} in {}".format(content, target_language))
        map_target_language = {"Afrikaans": "af", "Albanian": "sq", "Arabic": "ar", "Belarusian": "be",
                               "Bulgarian": "bg", "Catalan": "ca", "Chinese Simplified": "zh-CN",
                               "Chinese Traditional": "zh-TW", "Croatian": "hr", "Czech": "cs", "Danish": "da",
                               "Dutch": "nl", "English": "en", "Estonian": "et", "Filipino": "tl", "Finnish": "fi",
                               "French": "fr", "Galician": "gl", "German": "de", "Greek": "el", "Hebrew": "iw",
                               "Hindi": "hi", "Hungarian": "hu", "Icelandic": "is", "Indonesian": "id", "Irish": "ga",
                               "Italian": "it", "Japanese": "ja", "Korean": "ko", "Latvian": "lv", "Lithuanian": "lt",
                               "Macedonian": "mk", "Malay": "ms", "Maltese": "mt", "Norwegian": "no", "Persian": "fa",
                               "Polish": "pl", "Portuguese": "pt", "Romanian": "ro", "Russian": "ru", "Serbian": "sr",
                               "Slovak": "sk", "Slovenian": "sl", "Spanish": "es", "Swahili": "sw", "Swedish": "sv",
                               "Thai": "th", "Turkish": "tr", "Ukrainian": "uk", "Vietnamese": "vi", "Welsh": "cy",
                               "Yiddish": "yi"}

        if map_target_language.get(target_language):
            target_language_id = map_target_language.get(target_language)
            result = translator.translate(content, dest=target_language_id)

            return "{} in {} is {}".format(content, target_language, result.text)
        else:
            return "Unknown {} language".format(target_language)


class ActionTranslate(FormAction):

    @staticmethod
    def required_fields():
        return [
            FreeTextFormField("content"),
            EntityFormField("target_language", "target_language")
        ]

    def submit(self, dispatcher, tracker, domain):
        translate_api = TranslateAPI()
        translate_result = translate_api.search(tracker.get_slot("content"), tracker.get_slot("target_language"))
        dispatcher.utter_message(translate_result)

        return [SlotSet("translate_results", translate_result)]

    def name(self):
        return 'action_translate'


class Reset(Action):
    def name(self):
        # type: () -> Text
        return "action_reset"

    def run(self, dispatcher, tracker, domain):
        # type: (Dispatcher, DialogueStateTracker, Domain) -> List[Event]
        dispatcher.utter_template("utter_default")
        return [SlotSet("content", None), SlotSet("target_language", None)]
