from rasa_core.channels.channel import InputChannel, OutputChannel
from rasa_core.channels.channel import UserMessage
from rasa_core.channels.console import ConsoleOutputChannel
from fbmessenger import (
    BaseMessenger, elements, MessengerClient, attachments)
import logging

logger = logging.getLogger(__name__)


class BotlangMessage(BaseMessenger):
    """Implement a fbmessenger to parse incoming webhooks and send msgs."""

    def __init__(self, page_access_token, on_new_message):
        logging.info("Channel __init__ page_access_token {}".format(page_access_token))
        self.page_access_token = page_access_token
        self.on_new_message = on_new_message
        super(BotlangMessage, self).__init__(self.page_access_token)

    @staticmethod
    def _is_audio_message(message):
        """Check if the users message is a recorced voice message."""
        return (message.get('message') and
                message['message'].get('attachments') and
                message['message']['attachments'][0]['type'] == 'audio')

    @staticmethod
    def _is_user_message(message):
        """Check if the message is a message from the user"""
        return (message.get('message') and
                message['message'].get('text') and
                not message['message'].get("is_echo"))

    def message(self, message):
        """Handle an incoming event from the fb webhook."""
        if self._is_user_message(message):
            text = message['message']['text']
        elif self._is_audio_message(message):
            attachment = message['message']['attachments'][0]
            text = attachment['payload']['url']
        else:
            logger.error("Received a message from facebook that we can not "
                         "handle. Message: {}".format(message))
            return

        self._handle_user_message(text, self.get_user_id())

    def postback(self, message):
        """Handle a postback (e.g. quick reply button)."""

        text = message['postback']['payload']
        self._handle_user_message(text, self.get_user_id())

    def _handle_user_message(self, text, sender_id):
        """Pass on the text to the dialogue engine for processing."""

        out_channel = BotlangOutputChannel(self.client)
        user_msg = UserMessage(text, out_channel, sender_id)
        logging.info("BotlangOutputChannel UserMessage {}".format(text))
        try:
            self.on_new_message(user_msg)
        except Exception:
            logger.exception("Exception when trying to handle webhook "
                             "for facebook message.")
            pass

    def delivery(self, message):
        """Do nothing. Method to handle `message_deliveries`"""
        pass

    def read(self, message):
        """Do nothing. Method to handle `message_reads`"""
        pass

    def account_linking(self, message):
        """Do nothing. Method to handle `account_linking`"""
        pass

    def optin(self, message):
        """Do nothing. Method to handle `messaging_optins`"""
        pass


class BotlangInputChannel(InputChannel):
    """Input channel base class.

    Collects messages from some source and puts them into the message queue."""

    def start_async_listening(self, message_queue):
        """Start to push the incoming messages from channel into the queue."""
        self._record_messages(message_queue.enqueue)

    def start_sync_listening(self, message_handler):
        """Should call the message handler for every incoming message."""
        self._record_messages(message_handler)

    def _record_messages(self, on_message):
        self.on_message = on_message

    def process_message(self, message):
        self.on_message(UserMessage(message, ConsoleOutputChannel(),
                                    "default"))

    def process_data_messenger(self, fb_access_token, data):
        logging.info("process_data_messenger {}".format(data))
        messenger = BotlangMessage(fb_access_token, self.on_message)
        messenger.handle(data)


class BotlangOutputChannel(OutputChannel):
    """A bot that uses fb-messenger to communicate."""

    def __init__(self, messenger_client):
        # type: (MessengerClient) -> None

        self.messenger_client = messenger_client
        super(BotlangOutputChannel, self).__init__()

    def send(self, recipient_id, element):
        """Sends a message to the recipient using the messenger client."""

        # this is a bit hacky, but the client doesn't have a proper API to
        # send messages but instead expects the incoming sender to be present
        # which we don't have as it is stored in the input channel.
        self.messenger_client.send(element.to_dict(),
                                   {"sender": {"id": recipient_id}},
                                   'RESPONSE')

    def send_text_message(self, recipient_id, message):
        """Send a message through this channel."""

        logger.info("Botlang Sending message: " + message)

        self.send(recipient_id, elements.Text(text=message))

    def send_image_url(self, recipient_id, image_url):
        """Sends an image. Default will just post the url as a string."""

        self.send(recipient_id, attachments.Image(url=image_url))

    def send_text_with_buttons(self, recipient_id, text, buttons, **kwargs):
        """Sends buttons to the output."""

        # buttons is a list of tuples: [(option_name,payload)]
        if len(buttons) > 3:
            logger.error("Facebook API currently allows only up to 3 buttons. "
                         "If you add more, all will be ignored.")
            self.send_text_message(recipient_id, text)
        else:
            self._add_postback_info(buttons)

            # Currently there is no predefined way to create a message with
            # buttons in the fbmessenger framework - so we need to create the
            # payload on our own
            payload = {
                "attachment": {
                    "type": "template",
                    "payload": {
                        "template_type": "button",
                        "text": text,
                        "buttons": buttons
                    }
                }
            }
            self.messenger_client.send(payload,
                                       {"sender": {"id": recipient_id}},
                                       'RESPONSE')

    def send_custom_message(self, recipient_id, elements):
        """Sends elements to the output."""

        for element in elements:
            self._add_postback_info(element['buttons'])

        payload = {
            "attachment": {
                "type": "template",
                "payload": {
                    "template_type": "generic",
                    "elements": elements
                }
            }
        }
        self.messenger_client.send(payload,
                                   self._recipient_json(recipient_id),
                                   'RESPONSE')

    @staticmethod
    def _add_postback_info(buttons):
        """Set the button type to postback for all buttons. Happens in place."""
        for button in buttons:
            button['type'] = "postback"

    @staticmethod
    def _recipient_json(recipient_id):
        """Generate the response json for the recipient expected by FB."""
        return {"sender": {"id": recipient_id}}
