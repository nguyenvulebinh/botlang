import os
import sys

current_dir = os.path.dirname(__file__)
parent_dir = os.path.dirname(current_dir)
sys.path.insert(0, parent_dir)
import tensorflow as tf
from rasa_core import utils
from rasa_core.agent import Agent
from rasa_core.policies.keras_policy import KerasPolicy
from rasa_core.policies.memoization import MemoizationPolicy
import logging
from src.config import *
from src.core.wit_interpreter import WitInterpreter
from src.core.channel import BotlangInputChannel, BotlangOutputChannel

tf.logging.set_verbosity(tf.logging.ERROR)
logger = logging.getLogger(__name__)

agent = None
agent_input_channel = None


def init_agent():
    global agent
    global agent_input_channel
    agent_input_channel = BotlangInputChannel()
    agent = Agent.load(MODEL_PATH, interpreter=WitInterpreter())
    agent.handle_channel(agent_input_channel)


def get_agent_input_channel():
    global agent_input_channel
    if agent_input_channel is None:
        init_agent()
    return agent_input_channel


def run_botlang_train_online(input_channel,
                             interpreter=WitInterpreter(),
                             domain_file=DOMAIN_FILE,
                             training_data_file=TRAINNING_DATA_PATH):
    utils.configure_colored_logging(loglevel="INFO")
    agent = Agent(domain_file,
                  policies=[MemoizationPolicy(max_history=2), KerasPolicy()],
                  interpreter=interpreter)

    training_data = agent.load_data(training_data_file)
    agent.train_online(training_data,
                       input_channel=input_channel,
                       batch_size=50,
                       epochs=200,
                       max_training_samples=300)

    return agent


def train_botlang(model_path=MODEL_PATH):
    utils.configure_colored_logging(loglevel="INFO")

    agent = Agent(DOMAIN_FILE,
                  policies=[MemoizationPolicy(), KerasPolicy()],
                  interpreter=WitInterpreter())

    training_data = agent.load_data(TRAINNING_DATA_PATH)

    agent.train(
        training_data,
        epochs=400,
        batch_size=100,
        validation_split=0.2
    )

    agent.persist(model_path)
