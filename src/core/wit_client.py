import requests


def parse(text):
    url = "https://api.wit.ai/message"

    querystring = {"q": text,
                   "verbose": True
                   }
    headers = {
        'authorization': "Bearer QJ3JNFRPMZXHF4IKBBGA5HY4YHLPSIDI",
        'cache-control': "no-cache"
    }

    response = requests.request("GET", url, headers=headers, params=querystring)

    return response.json()
