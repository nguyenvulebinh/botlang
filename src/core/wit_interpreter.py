from rasa_core.interpreter import NaturalLanguageInterpreter
from src.core import wit_client
import logging

class WitInterpreter(NaturalLanguageInterpreter):
    def parse(self, text):
        entities = []
        intent = None
        nlu_result = wit_client.parse(text)
        if nlu_result.get('entities'):
            for key in nlu_result.get('entities').keys():
                if key == 'intent':
                    intent = {
                        "name": nlu_result.get('entities').get('intent')[0].get('value'),
                        "confidence": nlu_result.get('entities').get('intent')[0].get('confidence')
                    }
                else:
                    entities.append({
                        "start": nlu_result.get('entities').get(key)[0].get('_start'),
                        "end": nlu_result.get('entities').get(key)[0].get('_end'),
                        "value": nlu_result.get('entities').get(key)[0].get('value'),
                        "entity": key
                    })
        result = {
            "text": text,
            "intent": intent,
            "entities": entities
        }
        logging.info('NaturalLanguageInterpreter: {}'.format(result))
        return result
