import os

current_dir = os.path.dirname(__file__)

WIT_ACCESS_KEY = 'J5AL72BVVI7NUTMBP2I4WBUXCNMVD4TS'

VERIFY_TOKEN = 'botlang_verify'
MODEL_PATH = current_dir + '/core/models/dialogue'
DOMAIN_FILE = current_dir + '/core/domain.yml'
TRAINNING_DATA_PATH = current_dir + '/core/data/'


# database_connector
AEROSPIKE_HOST = 'db.botdy.vn'
AEROSPIKE_PORT = 3000
AEROSPIKE_NAMESPACE = 'storage'
AEROSPIKE_CHAT_SET = 'botlang_chatlogs_'
AEROSPIKE_USERINFO_SET = 'botlang_users'

# pattern path
ABBREVIATION_PATH = current_dir + '/data_preprocess/pattern/abbreviation_word.txt'
FIX_PATH = current_dir + '/data_preprocess/pattern/fix_word.txt'
COST_PATH = current_dir + '/data_preprocess/pattern/cost_pattern.txt'
OTHER_PATTERN_PATH = current_dir + '/data_preprocess/pattern/other_pattern.txt'
SENTIMENT_PATH = current_dir + '/data_preprocess/pattern/sentiment.txt'
BECKSPORT_PATTERN_PATH = current_dir + '/data_preprocess/pattern/becksport_pattern.txt'
ADAYROI_PATTERN_PATH = current_dir + '/data_preprocess/pattern/adayroi_pattern.txt'

PAGE_ACCESS_TOKEN = {
    # languagechatbot: nguyenvulebinh
    "2035635600093193": "EAAFN5rQpC2ABAIneeri901bEZABVM3fUJJY9E45fKsZB2f3pCa34pXW8gM149a6ZAKp9A0vPqPzRu6G3ZAaTjNzn1Ym2JsIC48lkDQ3xZCVtwGaj7EvfvDHiYvPmfAQGjDew2MFXjZCZBZAkNoSrDnw3uibS1YIn86FRjdEpHZAkRAQHQ6C2otCKc",
    "417619165415130":"EAALaghKgpJ0BAPWzmMXtMCRQE0SQATelGj9df7BsjmZBijflUV9YPODrQ5iZCLQLphX1OYXRyAS0FUmdeSsMMEfJSpwkZBCe1P8xk0Bryrdh0w17vZBJEZBsYYa2OX52AUZAha5GVWxcV0jWgpFEGSZAHyQWkdNyX87H393arVt0V4IqKDfEO4T"
}
