import re
from src.config import *


def load_pattern_data(customer_pattern_path):
    with open(ABBREVIATION_PATH, encoding='utf-8') as file:
        for line in file:
            components = line.strip().split('__')
            abbreviation_data[components[0]] = components[1]

    with open(FIX_PATH, encoding='utf-8') as file:
        for line in file:
            components = line.strip().split('__')
            fix_data[components[0]] = components[1]

    with open(COST_PATH, encoding='utf-8') as file:
        for line in file:
            components = line.strip().split('__')
            cost_data[components[0]] = components[1]

    if customer_pattern_path is not None:
        with open(customer_pattern_path, encoding='utf-8') as file:
            for line in file:
                components = line.strip().split('__')
                customer_pattern_data[components[0]] = components[1]

    with open(OTHER_PATTERN_PATH, encoding='utf-8') as file:
        for line in file:
            components = line.strip().split('__')
            other_pattern_data[components[0]] = components[1]

    with open(SENTIMENT_PATH, encoding='utf-8') as file:
        for line in file:
            components = line.strip().split('__')
            setiment_data[components[0]] = components[1]


# load data
other_pattern_data = {}
customer_pattern_data = {}
abbreviation_data = {}
fix_data = {}
cost_data = {}
setiment_data = {}


def check_abbreviation(list_word):
    for i in range(len(list_word)):
        if list_word[i] in abbreviation_data:
            list_word[i] = abbreviation_data[list_word[i]]


def check_fix_text(text):
    tmp_text = text
    for p in fix_data:
        matchs = re.finditer(p, text)
        for match in matchs:
            if fix_data[p] == '1':
                replace_text = match.group().replace('k', ' không ')
                tmp_text = tmp_text.replace(match.group(), replace_text)
    return tmp_text


def check_phonenumber(text):
    phonenumber_patterns = [' (0|\+?84)(\d ?){9,11} ', '^(0|\+?84)(\d ?){9,11} ', ' (0|\+?84)(\d ?){9,11}$']
    phonenumber_list = []
    for phonenumber_pattern in phonenumber_patterns:
        matchs = re.finditer(phonenumber_pattern, text)
        for match in matchs:
            phonenumber_list.append(str(match.group()).strip())
            text = re.sub(phonenumber_pattern, ' <numberphone> ', text)
    return text, phonenumber_list


def check_cost(text):
    cost_list = []
    for p in cost_data:
        matchs = re.finditer(p, text)
        for match in matchs:
            text = text.replace(match.group(), ' <cost> ')
            string_cost = str(match.group()).strip()
            case = cost_data[p]
            sub_matchs = re.finditer('[0-9]+', string_cost)
            if case == '1':
                for sub_match in sub_matchs:
                    cost_list.append(int(sub_match.group()) * 1000000)
            elif case == '2':
                for sub_match in sub_matchs:
                    cost_list.append(int(sub_match.group()) * 1000)
            elif case == '3':
                count = 0
                cost = 0
                for sub_match in sub_matchs:
                    if count == 0:
                        cost += int(sub_match.group()) * 1000000
                    if count == 1:
                        sub_cost = sub_match.group()
                        if len(sub_cost) == 3:
                            cost += int(sub_cost) * 1000
                        elif len(sub_cost) == 1:
                            cost += int(sub_cost) * 100000
                        else:
                            cost += int(sub_cost)
                    count += 1
                cost_list.append(cost)
            elif case == '4':
                for sub_match in sub_matchs:
                    cost_list.append(int(sub_match.group()) * 1500000)
            elif case == '5':
                for sub_match in sub_matchs:
                    cost_list.append(int(sub_match.group()) * 100000)
            elif case == '6':
                cost_list.append(int(re.sub('[.,]', '', string_cost)))
    return text, cost_list


def check_other_pattern(list_word):
    other_pattern_dict = {}
    for p in other_pattern_data:
        for i in range(len(list_word)):
            match = re.search(p, list_word[i])
            if match is not None:
                if other_pattern_data[p] in other_pattern_dict:
                    other_pattern_dict[other_pattern_data[p]].append(list_word[i])
                else:
                    other_pattern_dict[other_pattern_data[p]] = [list_word[i]]
                list_word[i] = other_pattern_data[p]
    return other_pattern_dict


def check_sentiment(list_word):
    check_setiment_dict = {}
    for p in setiment_data:
        for i in range(len(list_word)):
            if p in list_word[i]:
                if setiment_data[p] in check_setiment_dict:
                    check_setiment_dict[setiment_data[p]].append(list_word[i])
                else:
                    check_setiment_dict[setiment_data[p]] = [list_word[i]]
                list_word[i] = setiment_data[p]
    return check_setiment_dict


def check_customer_pattern(list_word):
    customer_pattern_dict = {}
    for p in customer_pattern_data:
        for i in range(len(list_word)):
            match = re.search(p, list_word[i])
            if match is not None:
                if customer_pattern_data[p] in customer_pattern_dict:
                    customer_pattern_dict[customer_pattern_data[p]].append(match.group(1))
                else:
                    customer_pattern_dict[customer_pattern_data[p]] = [match.group(1)]
                list_word[i] = customer_pattern_data[p]
    return customer_pattern_dict


def change_text(text, customer_pattern_path=None):
    load_pattern_data(customer_pattern_path)
    abbreviate = {}
    text = check_fix_text(text)

    check_phonenumber_result = check_phonenumber(text)
    text = check_phonenumber_result[0]
    if len(check_phonenumber_result[1]) > 0:
        abbreviate['<numberphone>'] = check_phonenumber_result[1]

    check_cost_result = check_cost(text)
    text = check_cost_result[0]
    if len(check_cost_result[1]) > 0:
        abbreviate['<cost>'] = check_cost_result[1]

    # abbreviation
    list_word = []
    for w in re.compile('[ \n\t]').split(text):
        if len(w) > 0:
            list_word.append(w)
    check_abbreviation(list_word)
    if customer_pattern_path is not None:
        abbreviate.update(check_customer_pattern(list_word))
    abbreviate.update(check_other_pattern(list_word))
    abbreviate.update(check_sentiment(list_word))

    return {'message': ' '.join(list_word), 'abbreviate': abbreviate}


if __name__ == '__main__':
    print(change_text('sdt toi la +841652375277 '
                      'https://www.becksport.vn/giay-phui-khau-de-beck-trang-soc-do \nva '
                      'https://work-19147557.facebook.com/chat/t/1767244459986592 1 \nso la 0169 526 '
                      '9189 toi co tien 200.000 hoac 1 triệu 1 hoặc 100,000,000'))
    print(change_text('có size 20k ? abc xyz có size 23k :))'))
