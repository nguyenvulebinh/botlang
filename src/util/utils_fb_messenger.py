# -*- coding: utf-8 -*-
import requests
from src.config import *
import json
from src.util import utils_server
import traceback
import datetime
import logging


def send_message(sender_id, recipient_id, message_text):
    """
    Gui 1 message
    :param sender_id:  id nguoi gui
    :param recipient_id: id nguoi nhan
    :param message_text: Noi dung tin nhan
    :return: None
    """
    json_data = json.dumps({
        "recipient": {"id": sender_id},
        "message": {
            "metadata": "BOT_MESSENGE",
            "text": message_text
        }
    })

    utils_server.print_log("send_message", json_data)
    r = requests.post("https://graph.facebook.com/v3.1/me/messages",

                      params={"access_token": PAGE_ACCESS_TOKEN[recipient_id]},

                      headers={"Content-Type": "application/json"},

                      data=json_data, timeout=100)
    logging.info('send message: ' + str(datetime.datetime.now()))
    logging.info(r.text)


def send_button(sender_id, recipient_id, list_button, text_response):
    """
    Gui cau hoi kem button de nguoi dung tra loi
    :param sender_id: id nguoi gui
    :param recipient_id: id nguoi nhan
    :param list_button: danh sach button object lay tu func get_item_quick_reply
    :param text_response: Noi dung mo ta danh sach quick button
    :return: None
    """
    json_data = json.dumps({
        "recipient": {"id": sender_id},
        "message": {
            "metadata": "BOT_MESSENGE",
            "attachment": {
                "type": "template",
                "payload": {
                    "template_type": "button",
                    "text": text_response,
                    "buttons": list_button
                }
            }
        }
    })
    utils_server.print_log("send_button", json_data)
    requests.post("https://graph.facebook.com/v2.6/me/messages",

                  params={"access_token": PAGE_ACCESS_TOKEN[recipient_id]},

                  headers={"Content-Type": "application/json"},

                  data=json_data, timeout=100)


def send_quick_reply(sender_id, recipient_id, list_quick_reply, text_response):
    """
    Gui quick button de click nhanh
    :param sender_id: id nguoi gui
    :param recipient_id: id nguoi nhan
    :param list_quick_reply: danh sach button object lay tu func get_item_quick_reply
    :param text_response: Noi dung mo ta danh sach quick button
    :return: None
    """
    json_data = json.dumps({
        "recipient": {"id": sender_id},
        "message": {
            "metadata": "BOT_MESSENGE",
            "text": text_response,
            "quick_replies": list_quick_reply
        }
    })
    utils_server.print_log("send_quick_reply", json_data)
    requests.post("https://graph.facebook.com/v2.6/me/messages",

                  params={"access_token": PAGE_ACCESS_TOKEN[recipient_id]},

                  headers={"Content-Type": "application/json"},

                  data=json_data, timeout=100)


def setup_page(page_id):
    """THuc hien cai dat nut get_start, white_list domain, menu"""
    result = {
        "setup_get_start": setup_get_start(page_id),
        "setup_whitelist_domain": setup_whitelist_domain(page_id),
        "setup_menu": setup_menu(page_id)
    }
    return json.dumps(result)


def setup_menu(page_id):
    """THuc hien cai dat menu"""
    json_data = json.dumps({
        "persistent_menu": [
            {
                "locale": "default",
                "composer_input_disabled": False,
                "call_to_actions": [
                    {
                        "type": "postback",
                        "title": "🤖 Bắt đầu hội thoại",
                        "payload": "{\"type\": \"RESTART_BOT\"}"
                    },
                    {
                        "type": "postback",
                        "title": "📞 Kết nối BP CSKH",
                        "payload": "{\"type\": \"CALL_HELP_DESK\"}"
                    }
                ]
            },
            {
                "locale": "vi_VN",
                "composer_input_disabled": False,
                "call_to_actions": [
                    {
                        "type": "postback",
                        "title": "🤖 Bắt đầu hội thoại",
                        "payload": "{\"type\": \"RESTART_BOT\"}"
                    },
                    {
                        "type": "postback",
                        "title": "📞 Kết nối BP CSKH",
                        "payload": "{\"type\": \"CALL_HELP_DESK\"}"
                    }
                ]
            }
        ]
    })
    utils_server.print_log("setup_menu", json_data)
    r = requests.post("https://graph.facebook.com/v2.6/me/messenger_profile",

                      params={"access_token": PAGE_ACCESS_TOKEN[page_id]},

                      headers={"Content-Type": "application/json"},

                      data=json_data, timeout=100)
    return r.json()


def setup_get_start(page_id):
    """THuc hien cai nut get_start"""
    json_data = json.dumps({
        "get_started": {
            "payload": "{\"type\": \"GET_STARTED\"}"
        }
    })
    utils_server.print_log("setup_get_start", json_data)
    r = requests.post("https://graph.facebook.com/v2.6/me/messenger_profile",

                      params={"access_token": PAGE_ACCESS_TOKEN[page_id]},

                      headers={"Content-Type": "application/json"},

                      data=json_data, timeout=100)
    return r.json()


def setup_whitelist_domain(page_id):
    """THuc hien cai nut whitelist_domain"""
    json_data = json.dumps({
        "setting_type": "domain_whitelisting",
        "whitelisted_domains": ["https://github.com"],
        "domain_action_type": "add"
    })
    utils_server.print_log("setup_whitelist_domain", json_data)
    r = requests.post("https://graph.facebook.com/v2.6/me/messenger_profile",

                      params={"access_token": PAGE_ACCESS_TOKEN[page_id]},

                      headers={"Content-Type": "application/json"},

                      data=json_data, timeout=100)
    return r.json()


def remove_setup_page(page_id):
    """Xoa cac cai dat get_start, menu cho page"""
    json_data = json.dumps({
        "fields": [
            "get_started",
            "persistent_menu"
        ]
    })
    utils_server.print_log("remove_setup_page", json_data)
    r = requests.delete("https://graph.facebook.com/v2.6/me/messenger_profile",

                        params={"access_token": PAGE_ACCESS_TOKEN[page_id]},

                        headers={"Content-Type": "application/json"},

                        data=json_data, timeout=100)
    return json.dumps(r.json())


def get_item_quick_reply(item_button_text, question_id, answer_id, image_url):
    """
    Tao danh sach button object de tra ve cho func show_quick_reply
    :param item_button_text: Noi dung cua button
    :param question_id: id cua cau hoi
    :param answer_id: id cau tra loi
    :param image_url: anh dai dien cho nut
    :return: None
    """
    if image_url is None:
        return {
            "content_type": "text",
            "title": item_button_text,
            "payload": json.dumps({"type": "QUICK_REPLY", "question_id": question_id, "answer_id": answer_id,
                                   "item_button_text": item_button_text})
        }
    else:
        return {
            "content_type": "text",
            "title": item_button_text,
            "payload": json.dumps({"type": "QUICK_REPLY", "question_id": question_id, "answer_id": answer_id,
                                   "item_button_text": item_button_text}),
            "image_url": image_url
        }


def get_item_button(title, type_button, data):
    """
    Tao mot doi tuong button de hien thi
    :param title:
    :param type_button:
    :param data:
    :return:
    """
    if type_button == "web_url":
        return {
            "type": "web_url",
            "url": data,
            "webview_height_ratio": "tall",
            "messenger_extensions": True,
            "title": title
        }
    elif type_button == "postback":
        return {
            "type": "postback",
            "title": title,
            "payload": data
        }


def send_image(sender_id, recipient_id, image):
    """
    Hien thi 1 anh cho nguoi dung
    :param sender_id: id nguoi gui
    :param recipient_id: id nguoi nhan
    :param image: image url
    :return: None
    """
    json_data = json.dumps({
        "recipient": {"id": sender_id},
        "message": {
            "metadata": "BOT_MESSENGE",
            "attachment": {
                "type": "image",
                "payload": {
                    "url": image
                }
            }
        }
    })
    utils_server.print_log("send_image", json_data)
    requests.post("https://graph.facebook.com/v2.6/me/messages",
                  params={"access_token": PAGE_ACCESS_TOKEN[recipient_id]},
                  headers={"Content-Type": "application/json"},
                  data=json_data,
                  timeout=100)


def send_typing(sender_id, recipient_id):
    """
    Hien thi bieu tuong typing tren cua so chat
    :param sender_id: id nguoi gui
    :param recipient_id: id nguoi nhan
    :return: None
    """
    if not PAGE_ACCESS_TOKEN.get(recipient_id):
        return
    json_data = json.dumps({
        "recipient": {"id": sender_id},
        "sender_action": "typing_on"
    })
    utils_server.print_log("send_typing", json_data)
    requests.post("https://graph.facebook.com/v2.6/me/messages",

                  params={"access_token": PAGE_ACCESS_TOKEN[recipient_id]},

                  headers={"Content-Type": "application/json"},

                  data=json_data, timeout=100)


def user_info(sender_id, recipient_id):
    """
    Lay thong tin facebook nguoi dung
    :param sender_id: id nguoi gui
    :param recipient_id: id nguoi nhan
    :return: json thong tin facebook nguoi dung
    """
    try:
        r = requests.get(url=("https://graph.facebook.com/v2.12/" + sender_id),
                         params={"access_token": PAGE_ACCESS_TOKEN[recipient_id]},
                         timeout=100)
        utils_server.print_log("user_info", r.text)
        return r.text
    except:
        tb = traceback.format_exc()
        utils_server.print_log("user_info_error", tb)


def get_json_obj_chat_text(sender_id, recipient_id, timestamp, message):
    """
    Event object nguoi dung text
    :param sender_id: id nguoi gui
    :param recipient_id: id nguoi nhan
    :param message: message object json cua fb messenger
    :return: json object da duoc chuan hoa
    """
    return \
        {
            "from": "CUSTOMER",
            "status": "CHAT",
            "sender_id": sender_id,
            "recipient_id": recipient_id,
            "page_id": recipient_id,
            'timestamp': timestamp,
            "event": {
                "type": "EVENT_MESSAGE",
                "payload": {
                    "mid": message["mid"],
                    "text": message["text"],
                    "attachments": None,
                    "nlp": message.get("nlp")
                }
            },
            "text_content": message.get("text")
        }


def get_mid(message):
    if 'event' in message:
        event = message['event']
        if 'payload' in event:
            payload = event['payload']
            if 'mid' in payload:
                return payload['mid']
    return str(get_timestamp(message)) + str(get_sender_id(message)) + str(get_recipient_id(message))


def get_timestamp(message):
    if 'timestamp' in message:
        return message['timestamp']
    return 0


def get_sender_id(message):
    if 'sender_id' in message:
        return message['sender_id']
    return None


def get_recipient_id(message):
    if 'recipient_id' in message:
        return message['recipient_id']
    return None


def get_page_id(message):
    if 'page_id' in message:
        return message['page_id']
    return None


def get_text(message):
    if 'event' in message:
        event = message['event']
        if 'payload' in event:
            payload = event['payload']
            if 'text' in payload:
                return payload['text']
    return None


def get_fb_mid(message):
    if 'event' in message:
        event = message['event']
        if 'payload' in event:
            payload = event['payload']
            if 'mid' in payload:
                return payload['mid']
    return None
