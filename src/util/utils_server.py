import traceback
import base64

import json
import logging
import aerospike
import requests

from src.config import *
from src.util import message_preparation, utils_fb_messenger

import datetime

db_client = None


def handle_message(data_object):
    """
    :param data_object:
    :return:
    """
    message = check_exists_message(data_object)
    print_log("handle_message", str(message))
    if message:
        # Show typing before process logic engine
        sender_id = data_object.get("sender_id")
        recipient_id = data_object.get("recipient_id")
        # utils_fb_messenger.send_typing(sender_id, recipient_id)
        return {'action': 'success'}
    return {'action': 'false'}


def create_client():
    config = {'hosts': [(AEROSPIKE_HOST, AEROSPIKE_PORT)]}
    ae_client = aerospike.client(config)
    ae_client.connect()
    print('connect to aerospike: ' + str(AEROSPIKE_HOST) + '-' + str(AEROSPIKE_PORT))
    return ae_client


def print_log(title, content):
    """
    Hien thi log tren server app engine
    :param title:
    :param content:
    :return:
    """
    logging.info(title)
    logging.info(content)


def check_exists_message(message):
    global db_client
    if db_client is None:
        db_client = create_client()

    # check exists message for duplicate
    mid = utils_fb_messenger.get_mid(message)
    m_aekey = (AEROSPIKE_NAMESPACE, AEROSPIKE_CHAT_SET + datetime.datetime.now().strftime("%m%Y"), mid)

    logging.info('check_exists_message: {} {}'.format(m_aekey, str(json.dumps(message))))

    if db_client.exists(m_aekey)[1] is not None:
        return None

    # get user info
    sender_id = utils_fb_messenger.get_sender_id(message)
    page_id = utils_fb_messenger.get_page_id(message)
    user_key = (AEROSPIKE_NAMESPACE, AEROSPIKE_USERINFO_SET, sender_id)

    if db_client.exists(user_key)[1] is None:
        # get info from user info api
        user_info = utils_fb_messenger.user_info(sender_id, page_id)
        logging.info('new info: ' + user_info)
        user_info = json.loads(user_info)
        # save new info
        db_client.put(user_key, {'user_id': sender_id,
                                 'page_id': page_id,
                                 'user_info': user_info})

    else:
        # get info from database_connector
        user_info = db_client.get(user_key)[2]['user_info']
        logging.info('user_info: ' + str(user_info))

    # Append user_info to message
    message.update(user_info)

    text = utils_fb_messenger.get_text(message)
    if text:
        message.get('event').get('payload').update(message_preparation.change_text(text, ADAYROI_PATTERN_PATH))
    # # saving message
    db_client.put(m_aekey, {'customer msg': str(json.dumps(message))})
    return message
