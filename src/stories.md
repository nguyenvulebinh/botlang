## Generated Story 287912513112047830
* greeting
    - say_hello
* translate{"content": "fun", "target_language": "Korean"}
    - slot{"content": "fun"}
    - slot{"target_language": "Korean"}
    - action_translate
    - slot{"target_language": "Korean"}
    - slot{"translate_results": "fun in Korean is \uc7a5\ub09c"}
* thanks
    - say_thanks
* greeting
    - say_hello
* translate{"content": "love"}
    - slot{"content": "love"}
    - action_translate
    - slot{"translate_results": "love in Korean is \uc560\uc815"}
* in Japanese
    - slot{"target_language": "Japanese"}
    - action_translate
    - slot{"target_language": "Japanese"}
    - slot{"translate_results": "love in Japanese is \u611b"}
* thanks
    - say_thanks
* ask_what_can_do
    - say_bot_can_do

## Generated Story 5393502479717717777
* translate{"content": "t\u00f4i", "target_language": "English"}
    - slot{"content": "t\u00f4i"}
    - slot{"target_language": "English"}
    - action_translate
    - slot{"target_language": "English"}
    - slot{"translate_results": "t\u00f4i in English is I"}
    - export

## Generated Story 3788707841100218972
* ask_what_can_do
    - say_bot_can_do
* ask_name
    - say_name
* thanks
    - say_thanks
* translate{"content": "queen", "target_language": "Hindi"}
    - slot{"content": "queen"}
    - slot{"target_language": "Hindi"}
    - action_translate
    - slot{"target_language": "Hindi"}
    - slot{"translate_results": "queen in Hindi is \u0930\u093e\u0928\u0940"}
* in Vietnamese
    - slot{"target_language": "Vietnamese"}
    - action_translate
    - slot{"target_language": "Vietnamese"}
    - slot{"translate_results": "queen in Vietnamese is n\u1eef ho\u00e0ng"}
* thanks
    - say_thanks
* greeting
    - say_hello
    - export

## Generated Story 9044581963078781682
* thanks
    - say_thanks
    - export

## Generated Story -3762552522593547576
* greeting
    - say_hello
* ask_what_can_do
    - say_bot_can_do
* translate{"content": "king", "target_language": "Japanese"}
    - slot{"content": "king"}
    - slot{"target_language": "Japanese"}
    - action_translate
    - slot{"target_language": "Japanese"}
    - slot{"translate_results": "king in Japanese is \u30ad\u30f3\u30b0"}
* ok
    - action_reset
    - slot{"content": null}
    - slot{"target_language": null}
* translate{"content": "me", "target_language": "Vietnamese"}
    - slot{"content": "me"}
    - slot{"target_language": "Vietnamese"}
    - action_translate
    - slot{"target_language": "Vietnamese"}
    - slot{"translate_results": "me in Vietnamese is t\u00f4i"}
* anything
    - action_reset
    - slot{"content": null}
    - slot{"target_language": null}
* in Vietnamese
    - slot{"target_language": "Vietnamese"}
    - action_translate
    - slot{"target_language": "Vietnamese"}
    - slot{"requested_slot": "content"}
* 'ant'
    - slot{"content": "ant"}
    - action_translate
    - slot{"content": "'ant'"}
    - slot{"translate_results": "'ant' in Vietnamese is 'ki\u1ebfn'"}
* ok
    - action_reset
    - slot{"content": null}
    - slot{"target_language": null}
* greeting
    - say_hello
    - export

## Generated Story 5551134446662874511
* greeting
    - say_hello
* translate{"content": "happy"}
    - slot{"content": "happy"}
    - action_translate
    - slot{"requested_slot": "target_language"}
* Vietnamese
    - slot{"target_language": "Vietnamese"}
    - action_translate
    - slot{"target_language": "Vietnamese"}
    - slot{"translate_results": "happy in Vietnamese is vui m\u1eebng"}
* ok
    - action_reset
    - slot{"content": null}
    - slot{"target_language": null}
* thanks
    - say_thanks
* greeting
    - say_hello
* ask_name
    - say_name
* thanks
    - say_thanks
* 0
    - export

## Generated Story -2343313912645792248
* fuck
    - action_reset
    - slot{"content": null}
    - slot{"target_language": null}
* greeting
    - say_hello
* the hell
    - action_reset
    - slot{"content": null}
    - slot{"target_language": null}
* translate{"content": "fun", "target_language": "Vietnamese"}
    - slot{"content": "fun"}
    - slot{"target_language": "Vietnamese"}
    - action_translate
    - slot{"target_language": "Vietnamese"}
    - slot{"translate_results": "fun in Vietnamese is vui v\u1ebb"}
* ok
    - action_reset
    - slot{"content": null}
    - slot{"target_language": null}
* 'the hell'
    - slot{"content": "the hell"}
    - action_translate
    - slot{"requested_slot": "target_language"}
* vietnamese
    - slot{"target_language": "Vietnamese"}
    - action_translate
    - slot{"target_language": "Vietnamese"}
    - slot{"translate_results": "the hell in Vietnamese is \u0111\u1ecba ng\u1ee5c"}
* thanks
    - say_thanks
* ask_name
    - say_name
* 1
    - export

## Generated Story -728374825986660442
* in
    - action_reset
    - slot{"content": null}
    - slot{"target_language": null}
* translate{"content": "we here", "target_language": "Japanese"}
    - slot{"content": "we here"}
    - slot{"target_language": "Japanese"}
    - action_translate
    - slot{"target_language": "Japanese"}
    - slot{"translate_results": "we here in Japanese is \u79c1\u305f\u3061\u306f\u3053\u3053\u306b\u3044\u308b"}
* ok
    - action_reset
    - slot{"content": null}
    - slot{"target_language": null}
* 'let's go'
    - slot{"content": "let's go"}
    - action_translate
    - slot{"requested_slot": "target_language"}
* vietnamese
    - slot{"target_language": "Vietnamese"}
    - action_translate
    - slot{"target_language": "Vietnamese"}
    - slot{"translate_results": "let's go in Vietnamese is \u0110i n\u00e0o"}
* japanese
    - slot{"target_language": "Japanese"}
    - action_translate
    - slot{"target_language": "Japanese"}
    - slot{"translate_results": "let's go in Japanese is \u884c\u3053\u3046"}
* thanks
    - say_thanks
* ask_what_can_do
    - say_bot_can_do
* translate{"content": "love"}
    - slot{"content": "love"}
    - action_translate
    - slot{"translate_results": "love in Japanese is \u611b"}
* in Vietnamese
    - slot{"target_language": "Vietnamese"}
    - action_translate
    - slot{"target_language": "Vietnamese"}
    - slot{"translate_results": "love in Vietnamese is y\u00eau"}
* thanks
    - say_thanks
    - export

## Generated Story -1559339061284049234
* ah
    - action_reset
    - slot{"content": null}
    - slot{"target_language": null}
* translate{"content": "hulk", "target_language": "Japanese"}
    - slot{"content": "hulk"}
    - slot{"target_language": "Japanese"}
    - action_translate
    - slot{"target_language": "Japanese"}
    - slot{"translate_results": "hulk in Japanese is \u30cf\u30eb\u30af"}
* thanks
    - say_thanks
* translate{"content": "action"}
    - slot{"content": "action"}
    - action_translate
    - slot{"translate_results": "action in Japanese is \u30a2\u30af\u30b7\u30e7\u30f3"}
* in vietnamese
    - slot{"target_language": "Vietnamese"}
    - action_translate
    - slot{"target_language": "Vietnamese"}
    - slot{"translate_results": "action in Vietnamese is ho\u1ea1t \u0111\u1ed9ng"}
* thanks
    - say_thanks
* ok
    - action_reset
    - slot{"content": null}
    - slot{"target_language": null}
* in vietnamese
    - slot{"target_language": "Vietnamese"}
    - action_translate
    - slot{"target_language": "Vietnamese"}
    - slot{"requested_slot": "content"}
* 'the queen'
    - slot{"content": "the queen"}
    - action_translate
    - slot{"content": "'the queen'"}
    - slot{"translate_results": "'the queen' in Vietnamese is 'n\u1eef ho\u00e0ng'"}
* japanese
    - slot{"target_language": "Japanese"}
    - action_translate
    - slot{"content": "japanese"}
    - slot{"target_language": "Japanese"}
    - slot{"translate_results": "japanese in Japanese is \u65e5\u672c\u8a9e"}
* thanks
    - say_thanks
* ask_what_can_do
    - say_bot_can_do
* thanks
    - say_thanks
* ask_name
    - say_name
    - export

