# Botlang

### Build

```
docker build --tag botlang -f botlang.Dockerfile .
```

### Run

```
docker run --detach \
--name botlang \
--volume "$PWD":/workspace \
--publish 8800:8800 \
botlang:latest
```